import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class Bienvenida extends JFrame implements ActionListener{

 private JTextField textfield1;
 private JLabel label1, label2, label3, label4;
 private JButton boton1;
 public static String texto = "";
  
 public Bienvenida(){
   setLayout(null);
   setDefaultCloseOperation(EXIT_ON_CLOSE);
   setTitle("Bienvenido");
   getContentPane().setBackground(new Color(255,0,0));
   setIconImage(new ImageIcon(getClass().getResource("images/icon.png")).getImage());
   
   ImageIcon imagen = new ImageIcon("images/logo-coca.png"); // Creamos el objeto de la imagen
   label1 = new JLabel(imagen); // Y Ese objeto lo colocamos dentro del Label
   label1.setBounds(25,15,300,150);
   add(label1);
  
   label2 = new JLabel("Sistema de Control Vacacional");
   label2.setBounds(35,135,300,30);
   label2.setFont(new Font("Andale Mono", 3, 18)); // Con esto determinamos que fuente y que tipo de texto utilizaremos. Entre las comillas tenemos la fuente, el 3 representa a la cursiva + negrita, 0 normal, 1 negrita, 2 cursiva. El 18 son los pixeles
   label2.setForeground(new Color(255,255,255)); // Esto define el color del texto
   add(label2);

   label3 = new JLabel("Ingrese su nombre");
   label3.setBounds(45,212,200,30);
   label3.setFont(new Font("Andale Mono", 1, 12)); 
   label3.setForeground(new Color(255,255,255));
   add(label3);

   label4 = new JLabel("�2020 The Coca-Cola Company");
   label4.setBounds(85,375,300,30);
   label4.setFont(new Font("Andale Mono", 1, 12)); 
   label4.setForeground(new Color(255,255,255));
   add(label4);
   
   textfield1 = new JTextField();
   textfield1.setBounds(45,240,255,25);
   textfield1.setBackground(new Color(224,224,224)); // Esto cambia el color de fondo del Textfield
   textfield1.setFont(new Font("Andale Mono", 1, 14)); 
   textfield1.setForeground(new Color(255,0,0));
   add(textfield1);

   boton1 = new JButton("Ingresar");
   boton1.setBounds(125,280,100,30);
   boton1.setBackground(new Color(255,255,255));
   boton1.setFont(new Font("Andale Mono", 1, 14));
   boton1.setForeground(new Color(255,0,0));
   boton1.addActionListener(this);
   add(boton1);
 }
 
   
 public void actionPerformed(ActionEvent e){
   if(e.getSource() == boton1){
     texto = textfield1.getText().trim(); // Recordemos que con el metodo .getText() tomamos el texto introducido, y el metodo .trim() elimina los espacios que haya escrito el usuario, dejando solamente el texto escrito.
     if(texto.equals("")){ // Este metodo se utiliza para verificar si el usuario escribio alg�n texto, las comillas sin nada entre ellas representan, precisamente, no hay texto.
       JOptionPane.showMessageDialog(null, "Debes ingresar tu nombre.");
     } else {
       Licencia ventanalicencia = new Licencia(); //Estas cinco lineas de codigo fueron sacadas del metodo main de Licencia, y se utiliza para pasar de la ventana bienvenida a la de Terminos y condiciones.
       ventanalicencia.setBounds(0,0,600,360);
       ventanalicencia.setVisible(true); // Con esto hacemos que se haga visible la ventana de terminos y condiciones.
       ventanalicencia.setResizable(false);
       ventanalicencia.setLocationRelativeTo(null);
       this.setVisible(false); // Y con esto hacemos que desaparezca nuestra interfaz Bienvenida para pasar a la de Terminos y condiciones.
     }
   }
 }

 public static void main(String args[]){
   Bienvenida ventanabienvenida = new Bienvenida();
   ventanabienvenida.setBounds(0,0,350,450);
   ventanabienvenida.setVisible(true);
   ventanabienvenida.setResizable(false);
   ventanabienvenida.setLocationRelativeTo(null); 
 }
}